using System;
using System.Collections.Generic;
using Jypeli;
using Jypeli.Assets;
using Jypeli.Controls;
using Jypeli.Widgets;

/// @author Aleksi Puttonen
/// @version 09.02.2020
/// <summary>
/// 
/// </summary>

public class Click_Hoops : PhysicsGame
/// <summary>
/// 
/// </summary>
{
    DoubleMeter countDown;
    IntMeter pointCounter;
    DoubleMeter forceMeter;
    Timer timer;
    Image taustakuva = LoadImage("Kenttä_pixel");

    bool throwing;
    PhysicsObject basketball;
    PhysicsObject levy;
    PhysicsObject floor;
    PhysicsObject ring;
    PhysicsObject hoop;

    PlatformCharacter player;
    Animation heittoanimaatio;




    private Image[] heittokuvat = LoadImages("väritetty5", "väritetty2");

    public override void Begin()
    {
        MakeWorld();
    }


    public void Kori()
    {
        Image korinKuva = LoadImage("kori");
        GameObject kori = new GameObject(640, 360);
        kori.Image = korinKuva;
        kori.X = 300;
        kori.Y = -30;
        Add(kori);
        levy = PhysicsObject.CreateStaticObject(5.0, 95.0);
        levy.Shape = Shape.Rectangle;
        levy.Y = kori.Top - 55;
        levy.X = kori.X - 63;
        Add(levy);

        ring = PhysicsObject.CreateStaticObject(1, 3);
        ring.Shape = Shape.Rectangle;
        ring.X = kori.X - 120;
        ring.Y = kori.Top - 87;
        ring.IsVisible = false;
        Add(ring);

        hoop = PhysicsObject.CreateStaticObject(37, 2);
        hoop.Top = ring.Bottom;
        hoop.X = ring.X + 25;
        hoop.IgnoresCollisionResponse = true;
        Add(hoop);
    }

    public void Player()
    {
        player = new PlatformCharacter(100, 200);
        player.X = -300;
        player.Y = -100;
        player.IgnoresPhysicsLogics = true;
        
        player.Image = LoadImage("väritetty2");
        heittoanimaatio = new Animation(heittokuvat);
        heittoanimaatio.FPS = 3;
        heittoanimaatio.StopOnLastFrame = true;
        
        Add(player);
    }


    public void MakeTimer()
    {
        countDown = new DoubleMeter(60);

        timer = new Timer();
        timer.Interval = 0.1;
        timer.Timeout += CountDownMethod;
        timer.Start();

        Label ShowTimer = new Label();
        ShowTimer.TextColor = Color.Gold;
        ShowTimer.Font = Font.DefaultHuge;
        ShowTimer.DecimalPlaces = 0;
        ShowTimer.BindTo(countDown);
        ShowTimer.X = -450;
        ShowTimer.Y = 350;
        Add(ShowTimer);
    }


    public void CountDownMethod()
    {
        countDown.Value -= 0.1;

        if (countDown.Value <= 0)
        {
            MessageDisplay.Add("Aika loppui...");
            timer.Stop();

            
            // Näytä pisteet ja uusi peli
        }
    }


    public void Controls()
    {

        Keyboard.Listen(Key.Space, ButtonState.Down, forceMeterValue, "Pidä pohjassa heiton voimakkuuden säätämiseen.");
        Keyboard.Listen(Key.Space, ButtonState.Pressed, animaatio, "latausanimaatio");
        Keyboard.Listen(Key.Space, ButtonState.Released, throwBall, null);
        PhoneBackButton.Listen(ConfirmExit, "Lopeta peli");
        Keyboard.Listen(Key.Escape, ButtonState.Pressed, ConfirmExit, "Lopeta peli");
    }


    public void Basketball()
    {
        basketball = new PhysicsObject(25, 25);
        Image koripallo = LoadImage("koripallo.png");
        basketball.Image = koripallo;
        basketball.Shape = Shape.Circle;
        basketball.Restitution = 0.5;
        Add(basketball);
        AddCollisionHandler(basketball, hoop, hitTheNet);
        resetBall();
    }


    public void MakeWorld()
    {
        Level.Background.Image = taustakuva;
        Level.Background.ScaleToLevelFull();
        Gravity = new Vector(0, -1000);
        Level.CreateBorders();
        floor = PhysicsObject.CreateStaticObject(Level.Width, 20);
        floor.Y = -200;
        floor.IsVisible = false;
        Add(floor);
        makeForceMeter();
        makePointCounter();
        MakeTimer();
        Player();
        Kori();
        Basketball();
        Controls();

    }

    public void makeForceMeter()
    {
        forceMeter = new DoubleMeter(0);
        forceMeter.MaxValue = 2000;
        BarGauge forceGauge = new BarGauge(10, 0.2 * Screen.HeightSafe);
        forceGauge.BorderColor = Color.Black;
        forceGauge.BarColor = Color.Gold;
        forceGauge.Angle = Angle.FromDegrees(270);

        forceGauge.BindTo(forceMeter);
        forceGauge.X = Level.Left + 100;
        forceGauge.Y = -300;
        Add(forceGauge);
    }
    public void resetBall()
    {
        basketball.IgnoresGravity = true;
        basketball.AngularVelocity = 0.0;
        basketball.Velocity = Vector.Zero;
        basketball.X = player.X;
        basketball.Y = player.Top;
        basketball.IsVisible = false;
        throwing = true;

    }
    // tällä saadaan bar toimimaan eli nostamaan arvoa kun spacebaria pidetään pohjassa.
    public void forceMeterValue()
    {
        forceMeter.Value += 30;
    }


    double countForce()
    {
        if (throwing)
        {
            return forceMeter.Value;
        }
        else
        {
            return 0;
        }
    }
    /// <summary>
    /// ohjelma joka tekee varsinaisen pallon heiton.
    /// Se käyttää throw metodia ja ottaa heiton voimaksi yllä olevan laskeVoima funktion palauttaman arvon. 
    /// </summary>
    
    public void animaatio()
    {
        player.Image = LoadImage("väritetty-ukko");
        resetBall();
    }

    void throwBall()
    {
        player.PlayAnimation(heittoanimaatio);
        throwing = true;
        basketball.IgnoresGravity = false;
        basketball.IsVisible = true;
        player.Throw(basketball, Angle.FromDegrees(70), countForce());
        forceMeter.Reset();
    }

   
    public void makePointCounter()
    {
        pointCounter = new IntMeter(0);

        Label pointScreen = new Label();
        pointScreen.X = Screen.Right - 40;
        pointScreen.Y = Screen.Top - 40;
        pointScreen.TextColor = Color.Gold;
        pointScreen.Font = Font.DefaultHuge;

        pointScreen.BindTo(pointCounter);
        Add(pointScreen);
    }


    public void hitTheNet(PhysicsObject basketball, PhysicsObject verkko)
    {
        pointCounter.Value += 1;
    }

}